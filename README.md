# Swarm in Docker

Swarm in a Docker container.

SWARM, Seismic Wave Analysis and Real-time Monitor, is a Java application designed to display and analyze seismic waveforms in real-time.
More info here:
- https://volcanoes.usgs.gov/software/swarm/download.php

## Quickstart

You can build the "Docker image" by your self or get the existing "Docker image" from Registry.
### Download the "Docker image" from Registry
First log in to GitLab’s Container Registry using your GitLab username and password:
```
$ docker login gitlab.rm.ingv.it:7654 -u <user> -p <password>
```

Pull the image:
```
$ docker pull gitlab.rm.ingv.it:7654/docker/swarm
```

Rename it:
```
$ docker tag gitlab.rm.ingv.it:7654/docker/swarm swarm && docker rmi gitlab.rm.ingv.it:7654/docker/swarm
```

Jump to "**Run docker**" section.

### Build "Docker image" by your self
```
$ git clone git@gitlab.rm.ingv.it:uf/swarm.git
$ cd swarm
$ docker build --tag swarm . 
```

To rebuild git-cloning Swarm Configuration:

```
docker build --build-arg CACHEGIT=$(date +%s) --tag swarm .
```

Jump to "**Run docker**" section.

### Run docker (Mac OSX)
#### XQuartz
Download and install **XQuartz**:
- https://www.xquartz.org

Enable flag: **Preferences** -> **Security** -> **Allow connections from network clients**.

#### Quick Run
Using the bash script `runSwarmInDocker.sh` all steps are automatic:
```
$ ./runSwarmInDocker.sh
```

#### Run by your self
Get your IP address and use it to start docker below:
```
$ ifconfig | grep "inet"
    . . .
	inet 10.0.4.75 netmask 0xff800000 broadcast 10.127.255.255
	. . .
$
```

run the command:
```
$ xhost + <your_ip_address>
```

open XQuartz App and start docker:
```
$ docker run -it --rm -e DISPLAY=<your_ip_address>:0 -v /tmp/.X11-unix:/tmp/.X11-unix swarm
```

Check **Swarm** version:
```
$ docker run -it --rm -e DISPLAY=<your_ip_address>:0 -v /tmp/.X11-unix:/tmp/.X11-unix swarm -version
```

### Run docker (Linux)
(under development)

# Contribute
Please, feel free to contribute.

# Credit
Swarm was developed in 2004 and 2005 at the Alaska Volcano Observatory by Dan Cervelli, Peter Cervelli, Thomas Parker, and Thomas Murray.

