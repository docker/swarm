# FROM debian:stretch
FROM alpine

LABEL maintainer="Matteo Quintiliani <matteo.quintiliani@ingv.it>"

ENV DEBIAN_FRONTEND=noninteractive
ENV INITRD No
ENV FAKE_CHROOT 1
ENV SWARM_BASEURL=https://volcanoes.usgs.gov/software/swarm/bin/
ENV SWARM_VERSION=swarm-3.0.0

# Install Swarm
WORKDIR /opt
RUN wget "${SWARM_BASEURL}/${SWARM_VERSION}-bin.zip" \
    && chmod +x /opt/${SWARM_VERSION}-bin.zip \
    && unzip ${SWARM_VERSION}-bin.zip 

# RUN apt-get update \
		# && apt-get dist-upgrade -y --no-install-recommends \
		# && apt-get install -y \
		# build-essential \
		# vim \
		# git \
		# telnet \
		# dnsutils \
		# wget \
		# default-jre \
		# unzip

RUN apk add --no-cache \
		procps \
		sudo \
		bash \
		openssh-client \
		git \
		ttf-dejavu \
		openjdk8-jre

# Set .bashrc
RUN echo "" >> /root/.bashrc \
     && echo "alias ll='ls -l --color'" >> /root/.bashrc \
     && . /root/.bashrc

# Copy GIT deploy key
RUN mkdir /root/.ssh
COPY id_rsa* known_hosts /root/.ssh/
RUN chmod 600 /root/.ssh/id_rsa \
    && chmod 644 /root/.ssh/id_rsa.pub \
    && chmod 644 /root/.ssh/known_hosts \
    && chmod 700 /root/.ssh/

# Clone 'earthworm/swarm' git repository
ARG CACHEGIT=1
RUN git clone https://docker_swarm:Gq5GpgGpmxRsiVvT6bxJ@gitlab.rm.ingv.it/earthworm/swarm.git
ARG CACHEGIT=0

# Copy INGV swarm configuration files from git
RUN cp -v /opt/swarm/Swarm* /opt/${SWARM_VERSION}/ \
    && cp -v /opt/swarm/swarm* /opt/${SWARM_VERSION}/ \
    && cp -v /opt/swarm/layouts/* /opt/${SWARM_VERSION}/layouts/ \
    && cp -v -R /opt/swarm/mapdata/PNG /opt/${SWARM_VERSION}/mapdata/ \
    && cp -v -R /opt/swarm/mapdata/italy /opt/${SWARM_VERSION}/mapdata/ \
    && cp -v -R /opt/swarm/mapdata/mormanno /opt/${SWARM_VERSION}/mapdata/ 

# Run Swarm
WORKDIR /opt/${SWARM_VERSION}
RUN sed -i'.bak' -e "s/windowMaximized=.*$/windowMaximized=false/g" Swarm.config
RUN chmod +x swarm.sh
ENTRYPOINT ["./swarm.sh"]
